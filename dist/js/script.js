"use stric";

//1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome,
//яка приймає рядок str і повертає true, якщо рядок є паліндромом
//(читається однаково зліва направо і справа наліво), або false в іншому випадку.

function isPalindrome(str) {
  return str === str.split("").reverse().join("");
}

console.log(isPalindrome("тут")); //true
console.log(isPalindrome("анна")); //true
console.log(isPalindrome("кіт")); //false

//2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок,
//який потрібно перевірити, максимальну довжину і повертає true, якщо
//рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший.
//Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
//funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
//funcName('checked string', 10); // false

function checkLength(str, maxLength) {
  return str.length <= maxLength;
}

console.log(checkLength("adsadsa", 24)); //true
console.log(checkLength("looongfortest", 10)); //false

//3. Створіть функцію, яка визначає скільки повних років користувачу.
//Отримайте дату народження користувача через prompt. Функція повина
//повертати значення повних років на дату виклику функцію.

let result;

const time = new Date();
let userInput = new Date(window.prompt("Ваша дата народження"));

result = time - userInput;

const years = Math.floor(result / (1000 * 60 * 60 * 24 * 365.25));

console.log(`Вам: ${years} років`);
